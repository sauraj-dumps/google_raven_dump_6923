## raven-user 12 SQ1D.211205.016.A1 7957957 release-keys
- Manufacturer: google
- Platform: gs101
- Codename: raven
- Brand: google
- Flavor: raven-user
- Release Version: 12
- Id: SQ1D.211205.016.A1
- Incremental: 7957957
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SQ1D.211205.016.A1/7957957:user/release-keys
- OTA version: 
- Branch: raven-user-12-SQ1D.211205.016.A1-7957957-release-keys
- Repo: google_raven_dump_6923


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
